<?php
try{
	include("config.php");
	$file_name = stripslashes($argv[1]);
	$jasId=stripslashes($argv[2]);
	$cornerMark=stripslashes($argv[3]);
	$spine_mm=stripslashes($argv[4]);
	$jobId=stripslashes($argv[5]);
	$orderId=trim(stripslashes($argv[6]));

	$basename = basename($file_name, '.pdf');
	$file_name=$app->RTP.$orderId."/".$file_name;

	$resolution=150;
	$spine=$spine_mm*0.0393701*$resolution;
	$cornerMark=$cornerMark*0.0393701*$resolution;
	$img = new Imagick();
	$img->setResolution($resolution,$resolution);
    $img->readImage($file_name);
    $img->setImageCompression(Imagick::COMPRESSION_JPEG); 
    $img->setImageCompressionQuality(95);	    
    $width = $img->getImageWidth();
    $height=$img->getImageHeight();
    $lineHeight=$height/100;
    $fontSize=10+($height/100);
    $textStart=$fontSize*10;
    $textBgSize=$fontSize*12;
    $draw = new ImagickDraw();

   ///backgrounds
   $draw->setFillColor('white');

   //middle background
   $draw->rectangle( ($width/2)-5,0,($width/2)+6,$lineHeight);
   $draw->rectangle( ($width/2)-5,$height,($width/2)+6,$height-$lineHeight);
   //spines background
   $draw->rectangle( ($width/2)-($spine/2)-5,0,($width/2)-($spine/2)+6,$lineHeight);
   $draw->rectangle( ($width/2)+($spine/2)-5,0,($width/2)+($spine/2)+6,$lineHeight);
   $draw->rectangle( ($width/2)-($spine/2)-5,$height,($width/2)-($spine/2)+6,$height-$lineHeight);
   $draw->rectangle( ($width/2)+($spine/2)-5,$height,($width/2)+($spine/2)+6,$height-$lineHeight);	   
   //top left backgrounds
   $draw->rectangle( 0,$cornerMark-5,$lineHeight,$cornerMark+6);
   $draw->rectangle( $cornerMark-5,0,$cornerMark+6,$lineHeight);
   //bottom left background
   $draw->rectangle( 0,$height-$cornerMark-5,$lineHeight,$height-$cornerMark+6);
   $draw->rectangle( $cornerMark-5,$height,$cornerMark+6,$height-$lineHeight);
   //top right background
   $draw->rectangle( $width-$cornerMark-5,0,$width-$cornerMark+6,$lineHeight);
   $draw->rectangle( $width,$cornerMark-5,$width-$lineHeight,$cornerMark+6);
   //bottom right background
   $draw->rectangle($width,$height-$cornerMark-1,$width-$lineHeight,$height-$cornerMark-6);
   $draw->rectangle($width-$cornerMark-5,$height,$width-$cornerMark+6,$height-$lineHeight);
   //barcode background
   $draw->rectangle( 0,($height/2),30,($height/2)+150);
   //text background
    $draw->rectangle( $width-30,($height/2)-$textBgSize, $width,($height/2)+$textBgSize);

   //middle
   $draw->setFillColor('red');
   $draw->rectangle( ($width/2),0,($width/2)+1,$lineHeight);
   	$draw->rectangle( ($width/2),$height,($width/2)+1,$height-$lineHeight);
   
   $draw->setFillColor('black');

   //spines
   $draw->rectangle( ($width/2)-($spine/2),0,($width/2)-($spine/2)+1,$lineHeight);
   $draw->rectangle( ($width/2)+($spine/2),0,($width/2)+($spine/2)+1,$lineHeight);
   $draw->rectangle( ($width/2)-($spine/2),$height,($width/2)-($spine/2)+1,$height-$lineHeight);
   $draw->rectangle( ($width/2)+($spine/2),$height,($width/2)+($spine/2)+1,$height-$lineHeight);
  
  //top left 
   $draw->rectangle( 0,$cornerMark,$lineHeight,$cornerMark+1);
   $draw->rectangle( $cornerMark,0,$cornerMark+1,$lineHeight);

   //bottom left
   $draw->rectangle( 0,$height-$cornerMark,$lineHeight,$height-$cornerMark+1);
   $draw->rectangle( $cornerMark,$height,$cornerMark+1,$height-$lineHeight);

   //top right
   $draw->rectangle( $width-$cornerMark,0,$width-$cornerMark+1,$lineHeight);
   $draw->rectangle( $width,$cornerMark,$width-$lineHeight,$cornerMark+1);

	//bottom right
   $draw->rectangle($width,$height-$cornerMark,$width-$lineHeight,$height-$cornerMark-1);
   $draw->rectangle($width-$cornerMark,$height,$width-$cornerMark+1,$height-$lineHeight);


   $img->drawImage($draw);
   //barcode
   	   $draw->setFontSize($fontSize);

   include("barcode.php");
   $barcode=new Imagick();
   $barcode->readImageBlob($barcodeImage);
   $barcode->rotateImage(new ImagickPixel(), 90); 
   $img->compositeImage($barcode, Imagick::COMPOSITE_DEFAULT, 0, ($height/2)-100); 
   $img->annotateImage($draw,5, ($height/2)+50,90, $jobId);
   //text
   $draw->setFontSize($fontSize);
   $img->annotateImage($draw, $width-8, ($height/2)+$textStart*1.2,-90, $spine_mm."mm    ".$basename);

   $img->setImageFormat('jpeg');
   umask(000);
   $img->writeImage($app->coverPath.$basename.'_2.jpg');
  //  $img->writeImage('/mnt/M/Cover2Samples/Original Files/MPC811_PBO0552350_PBMY_040_01_c_2.pdf');

    $img->destroy();

    $now=date("Y-m-d h:i:s");
    $query="update pps_jobs_automation_stage set finished_date=? where id=?";
	$stmt3=$app->mysqli->prepare($query);
	$stmt3->bind_param('si',$now,$jasId);
	$stmt3->execute();
}catch(Exception $e){
	$query="update pps_jobs_automation_stage set comment=? where id=?";
	$stmt3=$app->mysqli->prepare($query);
	$stmt3->bind_param('si',$e->getMessage(),$jasId);
	$stmt3->execute();
	$stmt3->close();
}
?>