<?php
class BatchController extends Controller {
/* 
CODES 
CODES
CODES
CODES
CODES
...
...
...  
*/

//Select the press device to set the press device for a job
    public function actionAdmin()
    {
        $model = new Batches();
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Batches'])){
            $model->attributes = $_GET['Batches'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }
    public function actionSelectPress(){
        $batchDevice=new BatchDevice();
        $batchDevice->pps_batches_id=$_GET['batchId'];
        echo $batchDevice->type=$_GET['type'];
        $batchDevice->status=1;
        $batchDevice->update_by=Yii::app()->user->id;
        $batchDevice->device_id=$_GET['printer'];
        /*activeTrigger will tell the model that you need to run the afterSave model
         function for this specific case. After save is override in customActiveRecord 
        */
        $batchDevice->activeTrigger($_GET['type']);
        $batchDevice->save();
        $jobs=Batches::batchJobs($_GET['batchId'],'list');
        foreach($jobs as $job){
            $job=Jobs::model()->findByPK($job);
            $note= "<p>sent ".$_GET['type']." to ".GeneralConfiguration::model()->findByPk($_GET['printer'])->name."</p>";
            // will log the user activity in the user logs. logNote is a function in customActiveRecord so each activity can log for each model as a different note type
            $job->logNote($note);
        }

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

/* 
CODES 
CODES
CODES
CODES
CODES
...
...
...	 
*/
