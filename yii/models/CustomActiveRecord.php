<?php
class CustomActiveRecord extends CActiveRecord {
    public $params;
    private $trigger=false;
    private $condition=false;
    //override afterSave function in CActiveRecord
    public function afterSave(){

        //check to run the afterSave if the trigger is active
        if($this->trigger==true){

            //find the controller request came from
            if(isset(Yii::app()->controller->id))
                $controller=Yii::app()->controller->id."/".Yii::app()->controller->action->id;
            else
                $controller='';
            //find all actions need to be done after save
                $triggers = Triggers::model()->findAll('controller = :controller', array(":controller" => $controller));
                //to all the actions needed to be done after save
                foreach($triggers as $trigger){

                    //each trigger can have condition when we call activeTrigger to activate the after save for model. here we check if condition is true
                    if(strstr($this->condition, $trigger['condition_value'])){
                        if(isset($trigger['action_model']) and !empty($trigger['action_model'])) {
                            $model=new $trigger['action_model']();
                           if(isset($model->params) and $this->params!='')
                                $model->params=$this->params;
                        } else {
                            throw new CHttpException(500, "No trigger has found for $controller, Please contact administrator to add stage trigger option");
                        }

                        //call the function from mentioned model in the database for this trigger
                        $parameters= explode(",", $trigger['action_parameters']);
                        call_user_func_array(array($model, $trigger['action_function']),$parameters);
                    }
                }
                //make the trigger deactive to stop afterSave for remaining scripts on the page
                $this->trigger=false;
            }
        
    }

    public function activeTrigger($condition=null){
            $this->trigger=true;
            $this->condition=$condition;
    }


    public function logNote($text,$id=null){
        if($id==null)
            $id=$this->id;
        $note=New Notes;
        $modelname=get_class($this);
        //array of models which we want to save the notes for them as a jobs model (jobs model is the main model in pps project)
        if(in_array($modelname, array("Batch")))
            $type="Jobs";
        else
            $type=$modelname;
        $note->type= $type;
        $note->create_by=Yii::app()->user->getId();
        $note->ref_id=$id;
        $note->notes=$text;
        $note->save();
    }
}