<?php

/**
 * This is the model class for table "pps_batches".
 *
 * The followings are the available columns in table 'pps_batches':
 * @property integer $id
 * @property integer $pps_customers_id
 * @property integer $batch_number
 * @property string $batch_serial
 * @property integer $status
 * @property string $config
 * @property integer $pps_batch_config_id
 * @property string $comments
 * @property string $create_date
 * @property string $close_date
 * @property string $update_date
 *
 * The followings are the available model relations:
 * @property BatchJobs[] $batchJobs
 * @property BatchConfig $ppsBatchConfig
 * @property Customers $ppsCustomers
 */
class Batches extends  CustomActiveRecord {
    const STATUS_OPEN = 1;
    const STATUS_CLOSE = 2;
    const STATUS_PRINTED= 3;
    static public $batchStatus = array(
        1 => 'OPEN',
        2 => 'CLOSE',
        3 => 'PRINTED',
    );
    public $jobs_count;
    public $product_code;
    public $filterSize;
    public $filterCurrentSize;
    public $filterProductCode;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pps_batches';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('pps_customers_id', 'required'),
            array('id, batch_number, pps_customers_id, status, pps_batch_config_id', 'numerical', 'integerOnly' => true),
            array('batch_serial', 'length', 'max' => 45),
            array('filterProductCode, filterCurrentSize, config, comments, create_date, close_date, update_date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('batch_number, status, filterProductCode, filterCurrentSize, create_date', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'batchJobs' => array(self::HAS_MANY, 'BatchJobs', 'pps_batches_id'),
            'config' => array(self::BELONGS_TO, 'BatchConfig', 'pps_batch_config_id'),
            'customer' => array(self::BELONGS_TO, 'Customers', 'pps_customers_id'),
            'devices' => array(self::HAS_MANY, 'BatchDevice', 'pps_batches_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'pps_customers_id' => 'Pps Customers',
            'batch_number' => 'Batch Number',
            'batch_serial' => 'Batch Serial',
            'status' => 'Status',
            'config' => 'Config',
            'pps_batch_config_id' => 'Pps Batch Config',
            'comments' => 'Comments',
            'create_date' => 'Create Date',
            'close_date' => 'Close Date',
            'update_date' => 'Update Date',
        );
    }

    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('batchJobs' => array('with' => array('job' => array('with' => 'product'))));
        $criteria->together = true;
        $criteria->group = 't.id';

        $this->filterCurrentSize = preg_replace("/\s+/", '', $this->filterCurrentSize);

        //to handle bigger than > and smaller than < in the filter as well
        if (!empty($this->filterCurrentSize) && preg_match("/([<>][=]?)?([\d]+)/", $this->filterCurrentSize, $matches)) {
            if (empty($matches[1])) {
                $operator = '=';
            } else {
                $operator = $matches[1];
            }
            $value = $matches[2];
            $criteria->having = "count(batchJobs.id) $operator $value";
        }

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.pps_customers_id', $this->pps_customers_id);
        $criteria->compare('batch_number', $this->batch_number, true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('config', $this->config, true);
        $criteria->compare('pps_batch_config_id', $this->pps_batch_config_id);
        $criteria->compare('comments', $this->comments, true);
        $criteria->compare('t.create_date', $this->create_date, true);
        $criteria->compare('close_date', $this->close_date, true);
        $criteria->compare('update_date', $this->update_date, true);
        $criteria->compare('product.product_code', $this->filterProductCode, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array('attributes' => array(
                    '*',
                    'filterCurrentSize' => array('asc' => 'count(batchJobs.id)', 'desc' => 'count(batchJobs.id) desc'),
                    'filterProductCode' => array('asc' => 'product.product_code', 'desc' => 'product.product_code desc'),
                )),
            'pagination' => array(
                'pageSize' => 50
            )
        ));
    }

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * 
     * @return string
     */
    public function getStatusName() {
        return BatchEngine::model()->batchStatuses[$this->status];
    }


/* 
CODES 
CODES
CODES
CODES
CODES
...
...
...  
*/

    public function getSize() {
        $config = CJSON::decode($this->config);
        $size = $config['setting']['size'];
        return $size;
    }

    public function getCurrentSize() {
        return BatchJobs::model()->countByAttributes(array('pps_batches_id' => $this->id));
    }

    public function getProductCodeString() {
        $config = CJSON::decode($this->config);
        $str = '';
        if (isset($config['criteria']['productCode']['item'])) {
            foreach ($config['criteria']['productCode']['item'] as $productCode) {
                $str .= "<code style='line-height:25px'>{$productCode}</code> ";
            }
        }

        return $str;
    }
    
    public function getPressActionScreen(){
       $config = CJSON::decode($this->config);
       $setting = $config['setting'];
       foreach($setting['device'] as $type => $device) {
           $typeName = EcommerceFileFormat::model()->findByAttributes(array('type' => $type, 'pps_customers_id' => $this->pps_customers_id))->label;
           $setting['device'][$type]['label'] = $typeName;
           $details = BatchDevice::model()->findByAttributes(array('pps_batches_id' => $this->id, 'type' => $type));
           $setting['device'][$type]['actionButton'] = '';
           if($details) {
               $setting['device'][$type]['status'] = $details->status;
               if($details->status == 2) {
                   $icon =  '<i class="fa fa-thumbs-o-up"></i>';
               } else if($details->status == 1) {
                   $icon =  '<i class="fa fa-clock-o"></i>';
               }
               $setting['device'][$type]['statusName'] = $icon . " " . BatchDevice::$statusName[$details->status];
               $setting['device'][$type]['deviceId'] = $details->device_id;
               $setting['device'][$type]['deviceName'] = GeneralConfiguration::model()->findByPk($details->device_id)->name;
               if($details->status == 1) {
                   $url = "/batch/updatePress/?status=2&batchDevice={$details->id}&batchId=$this->id";
                   $actionButton = CHtml::link("<i class=\"fa fa-check\"></i> Update", 'javascript:void(0)', array('class'=>'btn green btn-sm', 'url' => $url, 'onclick' => 'javascript:PPSBatch.actionHandler(event)')) . " ";
                   $setting['device'][$type]['actionButton'] = $actionButton;
               }
           } else {
               $setting['device'][$type]['status'] = '';
               $setting['device'][$type]['statusName'] = 'N/A';
           }
           $linkButton = "<p class='text-muted'>Not Ready</p>";
           if ($this->status == self::STATUS_CLOSE) {
                $linkButton = '';
                foreach ($device['item'] as $deviceId) {
                    $url = "/batch/selectPress/?type={$type}&batchId={$this->id}&printer={$deviceId}";
                    $name = GeneralConfiguration::model()->findByPk($deviceId)->name;
                    if ($deviceId == $device['default']) {
                        $defaultText = "*";
                        $color = "green";
                    } else {
                        $defaultText = '';
                        $color = "blue";
                    }
                    //$linkButton .= CHtml::link("<i class=\"fa fa-print\"></i> $name$defaultText", $url, array('class' => "btn {$color} btn-sm")) . " ";
                    $linkButton .= CHtml::link(
                            "<i class=\"fa fa-print\"></i> $name$defaultText",
                            'javascript:void(0)', 
                            array('class' => "btn {$color} btn-sm", 'url' => $url, 'onclick' => 'javascript:PPSBatch.actionHandler(event)')) . " ";
                }
            }
            $setting['device'][$type]['linkButton'] = $linkButton;
       }
       return Yii::app()->controller->renderPartial('_pressAction', array('model'=>$setting), true);
    }
    
    public function getPressActionButton()
    {
        $config = CJSON::decode($this->config);
        if($this->currentSize >= $this->size && $config['setting']['autoClose'] == true && $this->status == Batches::STATUS_OPEN) {
            //$html = CHtml::link('<i class="fa fa-times"></i> Close Batch', Yii::app()->createUrl('batch/closeBatch', array('id' => $this->id)), array('class' => 'btn red btn-sm'));
            $html = CHtml::link(
                    '<i class="fa fa-times"></i> Close Batch', 
                   'javascript:void(0)',
                    array('class' => 'btn red btn-sm', 'url' => Yii::app()->createUrl('batch/closeBatch', array('id' => $this->id))));
        } else {
            $html = "<p class='text-muted'>Not Available</p>";
        }
        return $html;
    }
}
