<?php
/* @var $this BatchController */
/* @var $model Batches */
$this->pageCrumbs = array(
    array("name" => "Home","link"=>"/"),
    array("name" => "Press Manager","link"=>"batch/admin"));
?>

<div class="row">
    <div class="col-md-12">
        <?php
        $statuses = Batches::$batchStatus;
        $statuses[""] = "ALL";
        //$model->status=Batches::STATUS_CLOSE;
        //$statuses=array_reverse($statuses);
        $this->widget('zii.widgets.grid.CGridView', array(
            'itemsCssClass' => 'table table-striped table-bordered table-hover dataTable',
            'pagerCssClass' => 'dataTables_paginate paging_simple_numbers',
            'pager' => array(
                'htmlOptions' => array(
                    'class' => 'pagination',
                    'style' => 'float: right',
                ),
                'header' => '',
                'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
                'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                'prevPageLabel' => '<i class="fa fa-angle-left"></i>',
                'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            ),
            'id' => 'pressGrid',
            'dataProvider' => $model->search(),
            'filter' => $model,
            'ajaxUpdate' => true,
            'afterAjaxUpdate' => 'function(id, data) {PPSBatch.initAjax()}',
            'columns' => array(
                array(
                    'name' => 'id',
                    'header' => 'Batch ID',
                    'type' => 'raw',
                    'value' => 'CHtml::link($data->id, "/job/preAndPostPressAdmin?Jobs[filterBatchId]=$data->id", array("target"=>"_blank"))',
                    'filter' => '<input type="text" class="form-control" name="Batches[id]" value="' . $model->id . '" >',
                    'filterHtmlOptions' => array('style' => 'width: 110px'),
                ),
                array(
                    'name' => 'batch_number',
                    'header' => 'Batch Number',
                    'type' => 'raw',
                    'value' => 'CHtml::link($data->batch_number, "/job/preAndPostPressAdmin?Jobs[filterBatchId]=$data->id", array("target"=>"_blank"))',
                    'filter' => '<input type="text" class="form-control" name="Batches[batch_number]" value="' . $model->batch_number . '" >',
                    'filterHtmlOptions' => array('style' => 'width: 150px'),
                ),
                array(
                    'name' => 'filterCurrentSize',
                    'header' => 'Current Size',
                    'type' => 'html',
                    'value' => '$data->currentSize . " / " . $data->size',
                    'filter' => '<input type="text" class="form-control" name="Batches[filterCurrentSize]" value="' . $model->filterCurrentSize . '" >',
                    'filterHtmlOptions' => array('style' => 'width: 120px'),
                ),
                array(
                    'name' => 'filterProductCode',
                    'header' =>'Product Code',
                    'type' => 'html',
                    'value' => '$data->productCodeString',
                    'filter' => '<input type="text" class="form-control" name="Batches[filterProductCode]" value="' . $model->filterProductCode . '" >',
                    'filterHtmlOptions' => array('style' => 'width: 200px'),
                ),
                array(
                    'name' => 'status',
                    'header' => 'Status',
                    'type' => 'raw',
                    'value' => 'Batches::$batchStatus[$data->status]',
                    'filter' =>  CHtml::dropDownList('Batches[status]', $model->status, $statuses, array('class' => 'bs-select form-control', 'data-live-search' => true)),//PPSHtml::bsDropDownList('Batches[status]', $model->status, $statuses, array('class' => 'form-control')),
                ),
                array(
                    'name' => 'create_date',
                    'header' => 'Date',
                    //'type' => 'raw',
                    'value' => 'date("Y-m-d", strtotime($data->create_date))',
                    'filter' => '<input type="text" class="form-control" name="Batches[create_date]" value="' . $model->create_date . '" >',
                    'filterHtmlOptions' => array('style' => 'width: 120px'),
                    'htmlOptions' => array('style' => 'text-align: center'),
                ),
                 array(
                    'header' => 'Action',
                    'type' => 'raw',
                    'value' => '$data->pressActionButton',
                    'filter' => PPSHtml::clearGridFilterButton("Clear Filter", "javascript:void(0)", 'pressGrid'),
                    'filterHtmlOptions' => array('style' => 'width: 120px'),
                    'htmlOptions' => array('style' => 'text-align: left'),
                ),
                array(
                    'header' => '<i class="fa fa-print"></i> Press Action',
                    'type' => 'raw',
                    'filter' => $this->renderPartial('_pressActionFilter', null, true),
                    //'value' => 'View::selectPress($data)',
                    'value' => '$data->pressActionScreen',
                    'htmlOptions' => array('style' => 'padding: 0px'),
                ),
            ),
        ));
        ?>
        <!-- end of batch table-->
    </div>

</div>
<script>
    PPSBatch = function(){
        return {
            actionHandler : function(event){
                var object = event.target;
                var url = $(object).attr('url');
                PPSBatch.requestAjax(url);
            },
            requestAjax : function(url) {
                $.ajax({
                    url: url,
                    type: "GET",
                }).done(function(){
                    $.fn.yiiGridView.update('pressGrid');
                });
            },
            initAjax : function() {
                PPSApp.initAjax();
            }
        }
    }();
</script>