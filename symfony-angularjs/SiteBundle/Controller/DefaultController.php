<?php

namespace Acme\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
	public function indexAction()
	{
		return $this->render('AcmeSiteBundle:Default:index.html.twig');
	}

	public function projectsAction()
	{
		return $this->render('AcmeSiteBundle:Default:projects.html.twig');
	}

	public function projectAction($id)
	{
		return $this->render('AcmeSiteBundle:Default:project.html.twig');
	}
	public function siteProjectsListJsonAction($start, $count, $country = null, $filter = null)
	{
		if ($country != null) {
			$countryCond = "and o.country='$country'";
		} else {
			$countryCond = "";
		}
		$limit       = $start . ',' . $count;
		$queryString = "SELECT o.orgName,p.id,p.title,p.description,p.createDate
                 FROM AcmeProjectBundle:Project p
                 JOIN p.organization o
                WHERE (o.orgName LIKE :filter
                 OR p.title LIKE :filter
                 )
                 AND p.approved=1  $countryCond
                  ORDER BY p.createDate
        ";
		$em          = $this->getDoctrine()->getManager();
		$query       = $em->createQuery($queryString)->setParameter('filter', "%$filter%")->setFirstResult($start)->setMaxResults($count);
		$projects    = $query->getResult();

		//die("a");
		$encoders    = array(new XmlEncoder(), new JsonEncoder());
		$normalizers = array(new GetSetMethodNormalizer());
		$serializer  = new Serializer($normalizers, $encoders);

		return new response($serializer->serialize($projects, 'json'));

	}
	
}
