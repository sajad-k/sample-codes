//Application decleration
var serve247App = angular.module('serve247App', ['ui.bootstrap']);
//Change Angularjs Start and End symbols
serve247App.config(function ($interpolateProvider) {
	$interpolateProvider.startSymbol('[[');
	$interpolateProvider.endSymbol(']]');
});

//ISO date Filter for angular js
serve247App.filter('dateToISO', function () {
	return function (input) {
		input = new Date(input).toISOString();
		return input;
	};
});

angular.module('serve247App').filter('cut', function () {
	return function (value, wordwise, max, tail) {
		if (!value) {
			return '';
		}

		max = parseInt(max, 10);
		if (!max) {
			return value;
		}
		if (value.length <= max) {
			return value;
		}

		value = value.substr(0, max);
		if (wordwise) {
			var lastspace = value.lastIndexOf(' ');
			if (lastspace != -1) {
				value = value.substr(0, lastspace);
			}
		}

		return value + (tail || ' …');
	};
});

//Projects Controller
serve247App.controller('projects', function ($scope, $http, $modal) {
	$scope.countries = ['Malaysia']
	$scope.filterCountry = 'Malaysia';
	//pagination settings
	$scope.currentPage = 1;
	$scope.itemsPerPage = 9;
	$scope.maxSize = 10;
	//sort Setting
	$scope.sortField = 'createDate';
	$scope.sortType = 'ASC';
	//filter Setting
	$scope.filterText = '';
	//load users list first page
	if (document.location.href.search('pendings') > 0) {
		$scope.group = 'pendings';
	}
	if (document.location.href.search('all') > 0) {
		$scope.group = 'all';
	}
	bigTotal = function () {

	};
	bigTotal();

	$scope.loadData = function () {
		if ($scope.filterText != "") {
			$http.get('/actions/siteProjectsTableCount/' + $scope.filterCountry + '/' + $scope.filterText).success(function (data) {
				//$scope.bigTotalItems = data;
			});
		}
		else {
			$http.get('/actions/siteProjectsTableCount/' + $scope.filterCountry).success(function (data) {
				$scope.bigTotalItems = data;

			});
		}
		if ($scope.filterText !== '') {
			$http.get('/actions/siteProjectsList/' + (($scope.currentPage - 1) * $scope.itemsPerPage) + '/' + $scope.itemsPerPage + '/' + $scope.filterCountry + '/' + $scope.filterText).success(function (data) {
				$scope.projectsList = data;
			});
		}
		else {
			$http.get('/actions/siteProjectsList/' + (($scope.currentPage - 1) * $scope.itemsPerPage) + '/' + $scope.itemsPerPage + '/' + $scope.filterCountry).success(function (data) {
				$scope.projectsList = data;
			});
		}
	};
	$scope.loadData();
	//sort function
	$scope.sort = function (field) {
		$scope.currentPage = 1;
		if ($scope.sortField == field) {
			if ($scope.sortType == 'ASC') {
				$scope.sortType = 'DESC';
			}
			else {
				$scope.sortType = 'ASC';
			}
		} else {
			$scope.sortField = field;
			$scope.sortTyp == 'ASC';
		}
		loadData();
	};

	//filter function
	$scope.filter = function (text) {
		$scope.currentPage = 1
		$scope.filterText = text;
		loadData('');
		bigTotal();

	};
	//Suspend User request function
	$scope.suspendProject = function (project) {
		$scope.projectInfo = angular.copy(project);
		console.log("here");
		//suspend project confirmation popup window (bootstrap directive)
		var modalInstance = $modal.open({
			templateUrl: 'suspendProjectPopUp',
			controller : ModalInstanceCtrl,
			resolve    : {
				projectInfo: function () {
					return $scope.projectInfo;
				}
			}
		});
		//suspend project confirmation result
		modalInstance.result.then(function (id) {
			angular.forEach($scope.projectsList, function (value, key) {
				if (value.id == id) {
					$scope.projectsList[key].locked = true;
				}
			});
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	//end of suspend project

	//unsuspend project request function
	$scope.unSuspendProject = function (project) {
		$scope.projectInfo = angular.copy(project);
		console.log("here");
		//ususpend Project confirmation popup window (bootstrap directive)
		var modalInstance = $modal.open({
			templateUrl: 'unSuspendProjectPopUp',
			controller : ModalInstanceCtrl,
			resolve    : {
				projectInfo: function () {
					return $scope.projectInfo;
				}
			}
		});
		//unsuspend Project confirmation result
		modalInstance.result.then(function (id) {
			angular.forEach($scope.projectsList, function (value, key) {
				if (value.id == id) {
					$scope.projectsList[key].locked = false;
				}
			});
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	//end of unsuspend project

	//delete Project request function
	$scope.deleteProject = function (project) {
		$scope.projectInfo = angular.copy(project);

		//delete Project confirmation popup window (bootstrap directive)
		var modalInstance = $modal.open({
			templateUrl: 'deleteProjectPopUp',
			controller : ModalInstanceCtrl,
			resolve    : {
				projectInfo: function () {
					return $scope.projectInfo;
				}
			}
		});
		//delete project confirmation result
		modalInstance.result.then(function (id) {
			angular.forEach($scope.projectsList, function (value, key) {
				if (value.id == id) {
					$scope.projectsList.splice(key, 1);
				}
				$scope.bigTotalItems = $scope.bigTotalItems - 1;
			});
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	//end of delete project

	//pagination change page
	$scope.setPage = function (pageNo) {
		console.log("update");
		$scope.currentPage = pageNo;
		$scope.loadData();
	};
});
//End of User Profiles Controller    

serve247App.controller('project', function ($scope, $http) {
	$scope.applyProject = function () {
		if (confirm("Are you sure?") == true) {
			window.location = "/projects/" + $scope.project.id + "/apply";
		}
	};
	$http.get('actions/project').success(function (data) {
		$scope.project = data;
	});
	 $http.get('/actions/siteProjectsList/0/4').success(function(data) {
            $scope.projectsList = data;
      }); 
});

serve247App.controller('enquiry', function ($scope, $http) {
	$scope.description = "";

	$scope.submitEnquiry = function () {
		$scope.errors = [];
		var error = false;
		if ($scope.description == '') {
			$scope.errors.push("description is required");
			error = true;
		}
		if (error) {
			return false;
		}
		$http.post('enquiry/submit', {'description': $scope.description})
			.success(function (data) {
				if (data == 200) {
					alert("your enquiry sent successfully");
					window.location = "../";
				}
				else {
					alert("something is wrong please try again");
				}
			}).error(function (data, status) {
				alert("something is wrong please try again");
			});
	}
});

var ModalInstanceCtrl = function ($http, $scope, $modalInstance, projectInfo) {
	console.log("modal");
	$scope.projectInfo = projectInfo;
	$scope.projectsList = projectInfo;

	$scope.suspend = function () {
		$http.get('actions/suspendProject/' + $scope.projectInfo.id).success(function (data) {
			if (data == '200') {
				$modalInstance.close($scope.projectInfo.id);
			} else {
				alert("something is wrong please cancel and try again");
			}

		});

	};

	$scope.unSuspend = function () {
		$http.get('actions/unSuspendProject/' + $scope.projectInfo.id).success(function (data) {
			if (data == '200') {
				$modalInstance.close($scope.projectInfo.id);
			} else {
				alert("something is wrong please cancel and try again");
			}

		});

	};

	$scope.projectDelete = function () {
		$http.get('actions/deleteProject/' + $scope.projectInfo.id).success(function (data) {
			if (data == '200') {
				$modalInstance.close($scope.projectInfo.id);
			} else {
				alert("something is wrong please cancel and try again");
			}

		});

	};
	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
};